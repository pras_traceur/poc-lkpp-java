/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100011
 Source Host           : localhost:5433
 Source Catalog        : poc-2020
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100011
 File Encoding         : 65001

 Date: 09/02/2020 22:09:48
*/


-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "users";
CREATE TABLE "users" (
  "id" int8 NOT NULL DEFAULT NULL,
  "nama" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "password" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "email" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "username" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "role_id" int8 DEFAULT NULL,
  "npwp" varchar COLLATE "pg_catalog"."default" DEFAULT NULL
)
;
ALTER TABLE "users" OWNER TO "postgres";

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO "users" VALUES (1, 'Super Admin', 'superadmin-1234', 'superadmin@adm.com', 'superadmin', 2, NULL);
INSERT INTO "users" VALUES (3, 'Admin', 'admin-1234', 'admin@gmail.com', 'admin', 4, '11.111.111.1-111.111');
INSERT INTO "users" VALUES (2, 'User', 'user-1234', 'user@gmail.com', 'user', 3, '11.111.111.1-111.111');
INSERT INTO "users" VALUES (6, 'User 2', 'user2-1234', 'user2@gmail.com', 'user2', 3, '11.111.111.1-111.111');
INSERT INTO "users" VALUES (18, 'User 3', 'user3-1234', 'user3@gmail.com', 'user3', 3, '11.111.111.1-111.111');
COMMIT;

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "users" ADD CONSTRAINT "u_username" UNIQUE ("username");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
