package models.workflow.instance;

import java.util.Collection;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class ScriptRunner {

	public static final ScriptEngine jsEngine = new ScriptEngineManager().getEngineByName("nashorn");// JDK 1.8

	public static Object runScript(ProcessInstance processInstance, Collection<VariableInstance> variableInstanceList, String script) throws Exception
	{	
		return runScript(processInstance, variableInstanceList, script, false);
	}
	
	private static Object runScript(ProcessInstance processInstance,Collection<VariableInstance> variableInstanceList, String script, boolean updateVariable) throws Exception
	{	
		Bindings bindings=jsEngine.createBindings();
		//jika variable tipe Date maka harus diubah menjadi new Date();
		for(VariableInstance variable: variableInstanceList)
		{
			String value=variable.value;
			// Handling WHITESPACE
			if (variable.value != null) {
				value = variable.value.trim();
			}
			String key=variable.variable_name;
			if("true".equalsIgnoreCase(value))
				bindings.put(key, true);
			else
			if("false".equalsIgnoreCase(value))
				bindings.put(key, false);
			else
				bindings.put(key, value);
		}
		jsEngine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
		Object obj=jsEngine.eval(script);
		if(updateVariable)
			//set variable back
		{
			String key = null;
			String value = null;
			Object o;
			for(VariableInstance var: variableInstanceList)
			{
				key=var.variable_name;
				// Handling NULL
				if ((o = bindings.get(key)) != null) {
					value=o.toString();
				}
				processInstance.setVariable(key, value);
			}
		}
		return obj;
	}
	
	
	public static Object runScriptUpdateVariable(ProcessInstance processInstance, Collection<VariableInstance> variableInstanceList, String script) throws Exception
	{
		return runScript(processInstance, variableInstanceList, script, true);
	}
}
