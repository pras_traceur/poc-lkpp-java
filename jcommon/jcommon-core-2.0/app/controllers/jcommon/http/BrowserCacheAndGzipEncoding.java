package controllers.jcommon.http;

import models.util.Config;
import org.apache.commons.io.IOUtils;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import play.cache.Cache;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Finally;
import play.mvc.Http.Header;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.zip.GZIPOutputStream;

/**Controller ini untuk membuat supaya sebuah page bisa menangani Header 'if-modified-since'
 * 1. Tambahkan anotasi @IfModifiedSince([durasi])
 * 2. Hanya bekerja untuk method 'GET'
 * @author Andik Yulianto
 */
public class BrowserCacheAndGzipEncoding extends Controller {

	private static final int GZIP_MIN_SIZE=1040;
	
	@Before(priority=9)
	protected static void beforeRender()
	{
		IfModifiedSince cache=getActionAnnotation(IfModifiedSince.class);
		//hanya untuk method 'GET'
		if(cache!=null && request.method.equalsIgnoreCase("GET"))
		{
    		Instant instant = Instant.now();
			String lastModified = Cache.get("IfModifiedSince" + request.url, String.class);
			if(lastModified==null)
			{
				lastModified= DateTimeFormatter.RFC_1123_DATE_TIME
			        .withZone(ZoneOffset.UTC)
			        .format(instant);
		  		Cache.set("IfModifiedSince" + request.url, lastModified, cache.value());
			}
	    	response.setHeader("Last-Modified", lastModified);
			Header header= request.headers.get("if-modified-since");
			if(header!=null)
			{
				String ims=header.value();
				if(ims.equals(lastModified))
				{
					//return not modified
					response.status=HttpResponseStatus.NOT_MODIFIED.getCode();
					response.out=new ByteArrayOutputStream(0);
				}
			}
		}
	}

	/**Lakukan gzip encoding jika client-nya support 
	 */
	@Finally(priority=3)
	protected static void gzipEncoding()
	{
		Header transferEncoding=response.headers.get("transfer-encoding");
		//kecuali chuncked
		if(transferEncoding!=null && transferEncoding.value().equals("chuncked"))
			return;
		Config config=Config.getInstance();
		if(config.gzipEncodingDisabled )
			return;
		String contentType=response.contentType;
		Header header=request.headers.get("accept-encoding");
		if((header!=null && header.value().contains("gzip"))
				&&(contentType !=null && (contentType.startsWith("text/") || contentType.startsWith("application/json"))))
		{
			byte[] aryIn=response.out.toByteArray();
			if(aryIn.length<=GZIP_MIN_SIZE)
				return;
			ByteArrayOutputStream out=new ByteArrayOutputStream();
			try{
				GZIPOutputStream gzip=new GZIPOutputStream(out);
				IOUtils.copy(new ByteArrayInputStream(aryIn), gzip);
				gzip.close();
			    response.setHeader("Content-Encoding", "gzip");
		        response.setHeader("Content-Length", String.valueOf(out.size()));
				response.out=out;
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
	}
}

