package models.jcommon.db.exporter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;

import models.jcommon.util.TempFileManager;
import play.Logger;

/**Membaca file-file yang digenerate oleh JsonExporter */
public class JsonExporterReader {

	private static final org.apache.log4j.Logger logger=org.apache.log4j.Logger.getLogger(JsonExporterReader.class);
	
	public static final String JSON_BZIP2=".result.bzip2";
	public static final String MD5=".md5.json";
	public static final String JSON_TEXT=".result.json";
	public static final String MERGE_JSON_TEXT=".merge.json";
	
	private String uniqueId;
	
	private Collection<String> resultCollection;
	
	public JsonExporterReader(String uniqueId)  {
		this.uniqueId=uniqueId;
	}
	
	public File getJsonData()
	{
		File file=TempFileManager.openFile(uniqueId + JSON_TEXT);
		if(file.exists())
			return file;
		else
			file=TempFileManager.openFile(uniqueId + JSON_BZIP2);
		return file;
	}
	
	public File getMD5File()
	{
		File file=TempFileManager.openFile(uniqueId + MD5);
		return file;
	}

	/**Lakukan perbandingan terhadap MD5PerRow dengan MD5PerRow yang didapat dari server.
	 * Sehingga  resultnya merupakan md5 yang ada di server saja atau ada di client saja -- tidak ada di keduanya
	 *  
	 * @param md5PerRowServer
	 * @throws IOException 
	 */
	public void compareMD5PerRows(String md5PerRowServer) throws IOException {
		Reader reader=new FileReader(getMD5File());		
		Collection<String> setMD5Client=convertMD5PerRowAsSet(reader);
		Collection<String> setMD5Server=convertMD5PerRowAsSet(new StringReader(md5PerRowServer));
				
		Collection<String> intersection=CollectionUtils.intersection(setMD5Client, setMD5Server);
		Logger.debug("[compareMD5PerRows] Jumlah Data => DCE: %s, LPSE: %s, intersection: %s", setMD5Server.size(), setMD5Client.size(), intersection);
		
		setMD5Client=CollectionUtils.subtract(setMD5Client, intersection);
		setMD5Server=CollectionUtils.subtract(setMD5Server, intersection);
		
		setMD5Client.addAll(setMD5Server);
		
		resultCollection=setMD5Client;
	}
	
	
	/**Dapatkan hasil akhir merge
	 * 
	 * @return
	 */
	public File getMergeJsonFile()
	{
		File file=null;
		if(resultCollection!=null)
		{
			/* setiap Elemen pada resultCollection berisi data dgn format
			 * [md5Row, pk1, pk1, ...]
			 * Kita harus membuang kolom md5Row  
			 */
			Set<String> rowsWithoutMD5=new HashSet<String>();
			for(String row : resultCollection)
			{
				int posComma=row.indexOf(',');
				if(posComma > 0)
				{
					String rowWithoutMD5="[" + row.substring(posComma+1);
					rowsWithoutMD5.add(rowWithoutMD5);
				}
			}
			
			
			file=TempFileManager.openFile(uniqueId + MERGE_JSON_TEXT);
			
			
		}
		return file;
	}
	
	//conversi MD5PerRow menjadi set
	private Set convertMD5PerRowAsSet(Reader reader) throws IOException
	{
		char[] cbuff=new char[1];
		StringBuilder str=new StringBuilder();
		HashSet<String> set=new HashSet<String>();
		int charIndex=0;
		do
		{
			int len=reader.read(cbuff);
			if(len<=0)
				break;
			char ch=cbuff[0];
			if(ch=='\n')
			{
				charIndex=-1;
				if(str.length()>0)
				{
					set.add(str.toString());
					str=new StringBuilder();
				}
			}
			else
				if(charIndex>=1)
					str.append(ch);
			charIndex++;			
		}while(true);
		return set;
		
	}
	

}
