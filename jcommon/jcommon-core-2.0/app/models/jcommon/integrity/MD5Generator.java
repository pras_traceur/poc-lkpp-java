package models.jcommon.integrity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**Generate MD5 for a folder tree
 * 
 * @author Mr. Andik
 *
 */
public class MD5Generator {
	
	private static Logger logger=Logger.getLogger(MD5Generator.class);
	private List<FileInfo> data=new ArrayList<FileInfo>();
	private String baseDir;
	private MessageDigest digest;
	
	/**Check integrity of epnweb
	 * args[0] root of folder
	 * args[1] file to store the result
	 * args[2] list of files to exclude; separated by ';'
	 * 
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException
	{
		if(args!=null)
		{
			MD5Generator si=new MD5Generator(args[0]);
			System.out.format("Processing: %s\n", args[0]);
			String exclude=null;
			if(args.length>2)
				exclude=args[2];
			si.generateMD5Digest(exclude);
			si.saveAsJson(args[0] +  "/" + args[1]);
			System.out.format("Done, result: %s\n", args[1]);
		}
	}
	
	public MD5Generator(String baseDir)
	{		
		
		this.baseDir=baseDir;
		digest=DigestUtils.getMd5Digest();
	}
	
//	/**membandingkan antara digest dari baseDir dengan data dari digestFile
//	 * 
//	 * @param digestFile
//	 * @throws IOException 
//	 */
//	public boolean compareDigest(String digestFile) throws IOException
//	{
//		String json=FileUtils.readFileToString(new File(digestFile));
//
//		Type typeOfHashMap = new TypeToken<List<FileInfo>>() { }.getType();
//
//		List<FileInfo> master =new Gson().fromJson(json, typeOfHashMap);
//		logger.info("[INTEGRITY] Checking system integrity");
//		boolean valid=true;
//	
//		//1. check apa ada file yg didelete/modified
//		master.removeAll(data);
//		for(FileInfo info: master) //jika masih ada artinya file telah dihapus/modified
//		{
//			valid=false;
//			logger.error("[INTEGRITY] [MISSING/MODIFIED] file has been deleted or modified: " + info);
//		}
//		
//		
//		
//		//2. check apa ada file baru?
//		master =new Gson().fromJson(json, typeOfHashMap);
//		data.removeAll(master); //hilangkan yang sama
//		for(FileInfo info: data) //jika masih ada artinya ini tambahan
//		{
//			valid=false;
//			logger.error("[ILLEGAL] file is not in master or has been modified: " + info);
//		}
//	
//		if(valid)
//			logger.info("[INTEGRITY] system integrity IS VALID");
//		else
//			logger.error("[INTEGRITY] system integrity IS INVALID. Please check log files");
//		return valid;
//	}
	
	public void generateMD5Digest(String exclude)
	{		
			final String[] aryExclude=exclude==null ? null:exclude.split(";");
			IOFileFilter filter=new IOFileFilter() {
			@Override
			public boolean accept(File file, String arg1) {
				return accept(file, null);
			}
			
			@Override
			public boolean accept(File file) {
				String path=file.getPath().replace('\\', '/');//ensure in linux format
				path=path.substring(baseDir.length());
				if(aryExclude==null)
					return true;
				for(String exclude: aryExclude)
				{
					if(path.startsWith(exclude))
						return false;
				}
				return true;
			}
		};
		File directory = new File(baseDir);
		if(!directory.isDirectory())
			return;
		Collection<File> cols=FileUtils.listFiles(directory, filter, TrueFileFilter.INSTANCE);
		int len=baseDir.length() + 1; //ensure not ends with '/' 
		StopWatch sw=new StopWatch();
		sw.start();
		int total=0;
		long size=0;
		for(File file: cols)
		{
			String name=file.getPath().substring(len);
			String md5=md5(file);
			total++;
			size+=file.length();
			data.add(new FileInfo(file.length(), md5, name));
			//logger.info(String.format("Name: %s, size: %,d, md5: %s", name, file.length(), md5));
		}
		sw.stop();
		
		logger.info(String.format("Preparing MD5Digest, dir: %s,  #File: %,d, size: %,d, duration: %s", baseDir, total, size, sw.toString()));		
	}
	
	
	/**
	 * Save data as JSON
	 * @param fileName
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void saveAsJson(String fileName) throws FileNotFoundException, IOException
	{
		Gson gson=new GsonBuilder().setPrettyPrinting().create();
		String json=gson.toJson(data);
		File file=new File(fileName);
		if(file.exists())
			file.delete();
		FileOutputStream out=new FileOutputStream(file);
		IOUtils.write(json, out);
		out.close();
		logger.info("Writing json to: " + fileName);
	}
	
	private String md5(File file)
	{
		digest.reset();
		try
		{
			InputStream is=new BufferedInputStream(new FileInputStream(file));
			byte[] buff=new byte[1024*10];
			
			while(true)
			{
				int size=is.read(buff);
				if(size>0)
					digest.update(buff, 0, size);
				else
					break;
			}
			byte result[]=digest.digest();
			is.close();
			return Hex.encodeHexString(result);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
