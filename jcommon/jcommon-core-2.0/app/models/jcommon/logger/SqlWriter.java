package models.jcommon.logger;

import java.sql.Connection;

import play.Logger;
import play.db.DB;

/**Class ini menulis log ke database via SQL
 * 
 * Log sebaiknya tidak disimpan di database, namun di file system
 */
@Deprecated
public class SqlWriter extends AbstractLogWriter {

	public void doJob() {
	//	Logger.debug("[JOB] SqlWriter.doJob");
			while(true)
			{
				try
				{	
					ActivityLog log= queue.poll();
					if(log==null)
						break;
					//save to database menggunakan Native SQL agar cepat
					Connection conn=DB.getConnection();
					conn.setAutoCommit(true);
					log.encryptData();
					java.sql.PreparedStatement st= conn.prepareStatement("INSERT INTO LOGGER.ACTIVITY_LOG(LOG_ID, LOG_TIME, USER_ID, HOST, DATA, URL) VALUES(nextval('logger.seq_activity_log'), ?, ?, ?, ?, ?)");
					st.setTimestamp(1, log.log_time);
					st.setString(2, log.user_id);
					st.setString(3, log.host);
					st.setString(4, log.data);
					st.setString(5, log.url);
					st.executeUpdate();
					st.close();
				//	String str=log.getDecryptedData();
//					Logger.debug("Writing to database: %s", str.toString());
				}
				catch(Exception e)
				{
					if(!(e instanceof java.lang.InterruptedException))
						Logger.error("[SqlWriter] Error while writing log: %s", e.toString());
				}
			}
		}

	
}
