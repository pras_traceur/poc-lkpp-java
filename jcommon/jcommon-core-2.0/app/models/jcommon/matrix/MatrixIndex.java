package models.jcommon.matrix;

/**
 * Index of a matrix
 * @author Mr. Andik
 *
 */
public class MatrixIndex {

	public int key;
	public String label;
	public MatrixIndex(int key, String label) {
		super();
		this.key = key;
		this.label = label;
	}
	
}
