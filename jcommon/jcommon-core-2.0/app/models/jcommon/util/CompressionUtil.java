package models.jcommon.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import play.Logger;
/**
 * Class ini menyediakan fasilitas untuk melakukan kompresi data
 * @author idoej
 *
 */
public class CompressionUtil {
	
	/**
	 * 
	 * @param bytes data yang ingin dikompres
	 * @return data hasil kompresi
	 * @throws Exception
	 */
	public static byte[] compress(byte[] bytes) {
		try {
	        ByteArrayOutputStream obj=new ByteArrayOutputStream();
	        GZIPOutputStream gzip = new GZIPOutputStream(obj);
	        gzip.write(bytes);
	        gzip.close();
	        byte[] ret = obj.toByteArray();
	        return ret;
		} catch (Exception e) {
			Logger.error(e, "GAgal compress data");
		}
		return null;
    }

	/**
	 * 
	 * @param str data yang ingin dikompres
	 * @return data hasil kompresi
	 * @throws Exception
	 */
	public static byte[] compress(String str){
        if (str == null || str.length() == 0) {
            return null;
        }
        return compress(str.getBytes());
    }

	/**
	 * 
	 * @param str data yang ingin didekompres
	 * @return data hasil dekompres
	 * @throws Exception
	 */
	public static byte[] decompress(String str) throws Exception {
		return decompress(str.getBytes("UTF-8"));
	}

	/**
	 * 
	 * @param data data yang ingin didekompres
	 * @return data hasil dekompres
	 * @throws Exception
	 */
	public static byte[] decompress(byte[] data) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			GZIPInputStream gzip = new GZIPInputStream(bais);
			byte[] buf = new byte[4096];
			int len = 0;
			while ((len = gzip.read(buf, 0, 4096)) > 0) {
				baos.write(buf, 0, len);
			}
			return baos.toByteArray();
		} catch (Exception e) {
			return null;
		}
	}
}
