h1. jcommon

Berisi fungsi-fungsi dasar jcommon

h2. SecureWSServer &  SecureWSClient 

p(note). Yaitu untuk webservice dengan security PKI (Public Key Infrastructure).

SecureWSServer &  SecureWSClient memerlukan 2 pasang kunci yang bisa digenerate oleh models.jcommon.secure.encrypt.CipherEngine.main(). 

h3. Penggunaan SecureWSClient

p(note). <b>Contoh:</b>
SecureWSClient ws= new SecureWSClient("http://www.inaproc.lkpp.go.id/WebService.daftarLpse");
ws.addParam("prov_id", "12");
ws.doPost(120); //120 timeout dalam milisecond
String responseString=ws.getResponseAsString();

h3. Penggunaan SecureWSServer

Ini digunakan untuk fungsi sebagai <i>server</i>. SecureWS akan melakukan parsing terhadap parameter-parameter yang dikirim

h3. Resumable Upload

Digunakan untuk proses upload yang bisa dilanjutkan. Di sisi server:
# Siapkan class yg extends <b>models.jcommon.blob.ResumableUploadListener</b> misal <i>upload.ResumableUploadListenerImpl</i>.
# Tambahkan nama class di atas pada <i>conf/application.conf</i>, dengan entry resumableUploadListener.class=<i>upload.ResumableUploadListenerImpl</i>
# Ketika proses upload selesai, controller akan memanggil method mada models.jcommon.blob.ResumableUploadListener
# Untuk menambahkan aspek keamanan, silakan extends <b>controllers.jcommon.http.ResumableUploadController</b>

Di sisi client, gunakan contoh di bawah ini

bc. 
@Test
public void doTest() throws IOException
{
  String fileName="D:/home/backup-database-development.sql";
  File file=new File(fileName);	
  
  String digest=DigestUtils.md5Hex(new FileInputStream(file));
  
  long fileSize=file.length();
  String url="http://localhost:9000/jcommon/uploadResumable?md5Hash=" + digest;
  WSRequest req= WS.url(url);
  //Step 1. Kirimkan request dengan header seperti ini
  req.setHeader("Content-Type", "application/octet-stream");
  req.setHeader("Range", "*/" + fileSize);
  HttpResponse resp= req.post();
  //Server memberi response dengan header ETag. Gunakan ini sebagai 'ID
  //	untuk upload
  
  //Step 2. Pada request-request berikutnya, 
  //gunakan queryString "?md5Hash=xxx&eTag=aaaa"
  String etag=resp.getHeader("ETag");
  url=url + "&eTag=" + etag;
  	
  WSRequest req2= WS.url(url);
  int start=0;
  int BLOCK_SIZE=1024*10;
  int stop;
  byte[] buff=new byte[BLOCK_SIZE];
  BufferedInputStream is=new BufferedInputStream(new FileInputStream(file));
  StopWatch sw=new StopWatch();
  sw.start();
  while(true)
  {
  	int byteRead=is.read(buff);
  	if(byteRead==-1)
  		break;			
  	req2.setHeader("Content-Type", "application/octet-stream");
  	stop=start+byteRead;
  	String range=String.format("%s-%s/%s", start,stop-1, fileSize);
  	//Step 2. Tambahkan Header "Range:a-b/c" di mana a: offset start,
  	//		b: offset end, c: file size
  	req2.setHeader("Range", range);			
  	start=stop;
  	byte[] body=Arrays.copyOfRange(buff, 0, byteRead);
  	req2.body=new ByteArrayInputStream(body);
  	HttpResponse resp2=req2.post();
  	if(!resp2.success())
  		throw new RuntimeException(resp2.getString());
  	Logger.debug("[CLIENT] Post  range: %s, status: [%s] %s", range, 
  		resp2.getStatusText(), resp2.getString());
  }
  sw.stop();
  Logger.debug("Upload DONE, duration: %s", sw );
  is.close();
}
