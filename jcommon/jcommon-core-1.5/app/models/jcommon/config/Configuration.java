package models.jcommon.config;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Transient;

import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import play.cache.Cache;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

@Table(name="CONFIGURATION")
public class Configuration extends BaseModel {
	
	/**
	 * Durasi Cache untuk kelas {@link Configuration} = 5 menit
	 */
	public static final String CACHE_DURATION = "5mn";

	@Id
	public String cfg_category;
	@Id
	public String cfg_sub_category;
	
	public String cfg_value;
	
	public String cfg_comment;
	
	// informasi key yang akan disimpan dalam play.cache.Cache
	@Transient
	public String toCache() {
		return cfg_category+"."+cfg_sub_category;
	}
	
	public static Configuration getConfiguration(String sub_category) {
		return find("cfg_category = 'CONFIG' and cfg_sub_category = ?", sub_category).first();
	}
	
	public static String getConfigurationValue(String sub_category) {
		return getConfigurationValue(sub_category, null);
	}

	/**
	 * Get configuration value. This class holds value in cache for 5 minute
	 * @param sub_category sub kategori
	 * @param defaultValue nilai
	 * @return nilai konfigurasi, jika cache ada ambil nilai dari cache
	 */
	public static String getConfigurationValue(String sub_category, String defaultValue) {
		if(CommonUtil.isEmpty(sub_category))
			return null;
		String value = Cache.get("CONFIG."+sub_category, String.class);
		if (CommonUtil.isEmpty(value)) {		
			value = Query.find("select cfg_value from configuration WHERE cfg_category = 'CONFIG' and cfg_sub_category = ?",String.class, sub_category).first();
			if (CommonUtil.isEmpty(value)) 
				value = defaultValue;
			else 						
				Cache.add("CONFIG."+sub_category, value, CACHE_DURATION);
		}
		return value;
	}
	
	/**
	 * Get Mail Template value. This class holds value in cache for 5 minute
	 * @param category kategori
	 * @param sub_category sub kategori
	 * @param defaultValue nilai
	 * @return nilai konfigurasi, jika cache ada ambil nilai dari cache
	 */

	public static String getTemplateValue(String mailTemplate) {
		// get from cache
		String value = Cache.get("EMAIL_TEMPLATE."+mailTemplate, String.class);
		if (value == null) {
			//cari di database
	
			Configuration configuration = find("cfg_category = ? and cfg_sub_category = ?", "EMAIL_TEMPLATE", mailTemplate.toString()).first();
		
			if(configuration ==null)
				value=null;
			else
			{
				value=configuration.cfg_value;
				Cache.add("EMAIL_TEMPLATE."+mailTemplate, configuration.cfg_value, CACHE_DURATION);
			}
		}
		return value;
	}

	/**
	 * Fungsi {@code updateConfigurationValue} digunakan untuk update nilai konfigurasi. Jika ada data di
	 * cache, update cache dengan nilai yang di-update.
	 *
	 * @param category kategori
	 * @param sub_category sub kategori
	 * @param value nilai update
	 * @return objek {@link Configuration} yang update
	 */
	public static Configuration updateConfigurationValue(String cfg_sub_category, String value) {
		Configuration configuration = find("cfg_category = 'CONFIG' and cfg_sub_category = ?", cfg_sub_category).first();
		if (configuration == null) {
			configuration = new Configuration();
			configuration.cfg_category = "CONFIG";
			configuration.cfg_sub_category = cfg_sub_category;
		}
		configuration.cfg_value = value;
		String currValue = Cache.get(configuration.toCache(), String.class);
		if (currValue != null) {
			// update cahe
			Cache.safeSet(configuration.toCache(), value, CACHE_DURATION);
		}
		configuration.save();
		return configuration;
	}
	
	public static boolean getBoolean(String config) {
		String val = getConfigurationValue(config);
		return val != null && Boolean.valueOf(val);
	}

	public static long getLong(String config) {
		String val = getConfigurationValue(config);
		if (val == null) 
			return 0;
		else if (val.trim().length() == 0)
			return 0;
		else return Long.valueOf(val);
	}

	public static int getInt(String config) {
		String val = getConfigurationValue(config);
		if (val == null)
			return 0;
		else if (val.trim().length() == 0)
			return 0;
		else
			return Integer.valueOf(val);
	}	

	public static Date getDate(String config) {
		String val = getConfigurationValue(config);
		Date date = null;
		try {
			if(val != null && !val.trim().isEmpty()){
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				date = (Date)formatter.parse(val);
			}
		} catch (ParseException e) {
			play.Logger.error("parsing date error :\n"+e);    
		}
		return date;
	}
}
