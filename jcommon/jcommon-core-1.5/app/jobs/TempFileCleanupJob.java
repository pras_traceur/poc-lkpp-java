package jobs;

import models.jcommon.util.TempFileManager;
import play.jobs.Job;

public class TempFileCleanupJob extends Job {
	
	public void doJob()
	{
		TempFileManager.deleteAllTempFiles();
	}
}
