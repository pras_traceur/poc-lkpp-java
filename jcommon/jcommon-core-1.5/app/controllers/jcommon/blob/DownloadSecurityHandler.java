package controllers.jcommon.blob;

import models.jcommon.blob.BlobTable;

public interface DownloadSecurityHandler {
	public boolean allowDownload(BlobTable secureIdBlobTable);
}
