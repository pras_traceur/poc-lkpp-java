package models;

import java.util.List;

//import play.db.jdbc.BaseTable;
//import play.db.jdbc.Id;
//import play.db.jdbc.Table;

import play.db.jpa.*;
import javax.persistence.*;

@Entity
@Table(name="roles")
public class Role extends Model {

    //@Id(function = "nextval", sequence = "seq_roles")
	//public Long id;
    public String name;

    public static List<Role> getAll()
	{
		return find("NOT name LIKE '?%' ORDER BY id desc").fetch();
    }
}